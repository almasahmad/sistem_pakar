package com.example.pakarpenyakit;

import android.app.Dialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.FileNotFoundException;
import java.lang.reflect.Array;
import java.util.Arrays;

public class MainActivity extends AppCompatActivity {

    private CheckBox cb1, cb2,cb3,cb4,cb5,cb6,cb7,cb8,cb9,cb10,cb11,cb12,cb13,cb14,cb15,cb16,cb17,cb18,cb19;
    private Button button;
    private EditText editText;

    int p1 = 0;
    int p2 = 0;
    int p3 = 0;
    int p4 = 0;
    int p5 = 0;
    int p6 = 0;
    int p7 = 0;
    int p8 = 0;
    int p9 = 0;
    int p10 = 0;
    int p11 = 0;
    int p12 = 0;
    int p13 = 0;
    int p14 = 0;
    int p15 = 0;
    int p16 = 0;
    int p17 = 0;
    int p18 = 0;
    int p19 = 0;
    int penyakit = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        cb1 = findViewById(R.id.cb1);
        cb2 = findViewById(R.id.cb2);
        cb3 = findViewById(R.id.cb3);
        cb4 = findViewById(R.id.cb4);
        cb5 = findViewById(R.id.cb5);
        cb6 = findViewById(R.id.cb6);
        cb7 = findViewById(R.id.cb7);
        cb8 = findViewById(R.id.cb8);
        cb9 = findViewById(R.id.cb9);
        cb10 = findViewById(R.id.cb10);
        cb11 = findViewById(R.id.cb11);
        cb12 = findViewById(R.id.cb12);
        cb13 = findViewById(R.id.cb13);
        cb14 = findViewById(R.id.cb14);
        cb15 = findViewById(R.id.cb15);
        cb16 = findViewById(R.id.cb16);
        cb17 = findViewById(R.id.cb17);
        cb18 = findViewById(R.id.cb18);
        cb19 = findViewById(R.id.cb19);

        button = findViewById(R.id.btn);

        editText = findViewById(R.id.ed);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{
                    if(cb1.isChecked()){
                        p1=1;
                    }else{
                        p1=0;
                    }
                    if(cb2.isChecked()){
                        p2=1;
                    }else{
                        p2=0;
                    }
                    if(cb3.isChecked()){
                        p3=1;
                    }else{
                        p3=0;
                    }
                    if(cb4.isChecked()){
                        p4=1;
                    }else{
                        p4=0;
                    }
                    if(cb5.isChecked()){
                        p5=1;
                    }else{
                        p5=0;
                    }
                    if(cb6.isChecked()){
                        p6=1;
                    }else{
                        p6=0;
                    }
                    if(cb7.isChecked()){
                        p7=1;
                    }else{
                        p7=0;
                    }
                    if(cb8.isChecked()){
                        p8=1;
                    }else{
                        p8=0;
                    }
                    if(cb9.isChecked()){
                        p9=1;
                    }else{
                        p9=0;
                    }
                    if(cb10.isChecked()){
                        p10=1;
                    }else{
                        p10=0;
                    }
                    if(cb11.isChecked()){
                        p11=1;
                    }else{
                        p11=0;
                    }
                    if(cb12.isChecked()){
                        p12=1;
                    }else{
                        p12=0;
                    }
                    if(cb13.isChecked()){
                        p13=1;
                    }else{
                        p13=0;
                    }
                    if(cb14.isChecked()){
                        p14=1;
                    }else{
                        p14=0;
                    }
                    if(cb15.isChecked()){
                        p15=1;
                    }else{
                        p15=0;
                    }
                    if(cb16.isChecked()){
                        p16=1;
                    }else{
                        p16=0;
                    }
                    if(cb17.isChecked()){
                        p17=1;
                    }else{
                        p17=0;
                    }
                    if(cb18.isChecked()){
                        p18=1;
                    }else{
                        p18=0;
                    }
                    if(cb19.isChecked()){
                        p19=1;
                    }else{
                        p19=0;
                    }

                    int g20 = 100 / 4;
                    int g21 = 100 / 3;
                    int g22 = 100 / 2;
                    int g23 = 100 / 3;
                    int g24 = 100 / 2;
                    int g25 = 100 / 4;
                    int g26 = 100 / 4;
                    int g27 = 100 / 2;
                    int g28 = 100 / 5;
                    int g29 = 100 / 2;
                    int g30 = 100 / 2;
                    int g31 = 100 / 2;
                    int g32 = 100 / 2;

                    int s20 = ((p1 * g20) + (p2 * g20) + (p4 * g20) + (p5 * g20));
                    int s21 = ((p4 * g21) + (p5 * g21) + (p6 * g21));
                    int s22 = ((p4 * g22) + (p7 * g22));
                    int s23 = ((p4 * g23) + (p8 * g23) + (p9 * g23));
                    int s24 = ((p8 * g24) + (p10 * g24));
                    int s25 = ((p4 * g25) + (p5 * g25) + (p9 * g25) + (p11 * g25));
                    int s26 = ((p4 * g26) + (p8 * g26) + (p11 * g26) + (p12 * g26));
                    int s27 = ((p4 * g27) + (p13 * g27));
                    int s28 = ((p1 * g28) + (p2 * g28) + (p3 * g28) + (p4 * g28) + (p5 * g28));
                    int s29 = ((p14 * g29) + (p15 * g29));
                    int s30 = ((p14 * g30) + (p16 * g30));
                    int s31 = ((p14 * g31) + (p17 * g31));
                    int s32 = ((p18 * g32) + (p19 * g32));

                    int g33 = 100 / 5;
                    int g34 = 100 / 5;
                    int g35 = 100 / 6;
                    int g36 = 100 / 3;
                    int g37 = 100 / 4;

                    int s33 = ((s20 * g33) + (s21 * g33) + (s22 * g33) + (s23 * g33) + (s29 * g33)) / 100;
                    int s34 = ((s20 * g34) + (s21 * g34) + (s22 * g34) + (s24 * g34) + (s30 * g34)) / 100;
                    int s35 = ((s20 * g35) + (s21 * g35) + (s22 * g35) + (s25 * g35) + (s26 * g35) + (s29 * g35)) / 100;
                    int s36 = ((s21 * g36) + (s27 * g36) + (s31 * g36)) / 100;
                    int s37 = ((s28 * g37) + (s22 * g37) + (s25 * g37) + (s32 * g37)) / 100;

                    int[] data = { s33, s34, s35, s36, s37 };
                    Arrays.sort(data);

                    String number = editText.getText().toString();
                    int result = Integer.parseInt(number);

                    if(data[4] > result){
                        penyakit  = data[4];
                    }else {
                        penyakit = 1;
                    }

                    final Dialog dialog = new Dialog(MainActivity.this);
                    dialog.setContentView(R.layout.muncul);
                    dialog.setTitle("TWOH.Co");

                    if (penyakit == s33) {
                        TextView text = (TextView) dialog.findViewById(R.id.tv_desc);
                        text.setText("Keracunan Staphylococcus aureus : " + s33 + "%\nKeracunan Jamur beracun : " + s34 + "%\nKeracunan Salmonellae : " + s35 + "%\nKeracunan Clostridium botulium : " + s36 + "%\nKeracunan Campylobacter : " + s37 + "%\n\nKesimpulan Penyakit : Keracunan Staphylococcus aureus");
                    }else if (penyakit == s34) {
                        TextView text1 = (TextView) dialog.findViewById(R.id.tv_desc);
                        text1.setText("Keracunan Staphylococcus aureus : " + s33 + "%\nKeracunan Jamur beracun : " + s34 + "%\nKeracunan Salmonellae : " + s35 + "%\nKeracunan Clostridium botulium : " + s36 + "%\nKeracunan Campylobacter : " + s37 + "%\n\nKesimpulan Penyakit : Keracunan Jamur Beracun");
                    }else if (penyakit == s35) {
                        TextView text2 = (TextView) dialog.findViewById(R.id.tv_desc);
                        text2.setText("Keracunan Staphylococcus aureus : " + s33 + "%\nKeracunan Jamur beracun : " + s34 + "%\nKeracunan Salmonellae : " + s35 + "%\nKeracunan Clostridium botulium : " + s36 + "%\nKeracunan Campylobacter : " + s37 + "%\n\nKesimpulan Penyakit : Keracunan Salmonellae");
                    }else if (penyakit == s36) {
                        TextView text3 = (TextView) dialog.findViewById(R.id.tv_desc);
                        text3.setText("Keracunan Staphylococcus aureus : " + s33 + "%\nKeracunan Jamur beracun : " + s34 + "%\nKeracunan Salmonellae : " + s35 + "%\nKeracunan Clostridium botulium : " + s36 + "%\nKeracunan Campylobacter : " + s37 + "%\n\nKesimpulan Penyakit : Keracunan Clostridium botulinum");
                    }else if (penyakit == s37) {
                        TextView text4 = (TextView) dialog.findViewById(R.id.tv_desc);
                        text4.setText("Keracunan Staphylococcus aureus : " + s33 + "%\nKeracunan Jamur beracun : " + s34 + "%\nKeracunan Salmonellae : " + s35 + "%\nKeracunan Clostridium botulium : " + s36 + "%\nKeracunan Campylobacter : " + s37 + "%\n\nKesimpulan Penyakit : Keracunan Campylobacter");
                    }else if (penyakit == 1){
                        TextView text5 = (TextView) dialog.findViewById(R.id.tv_desc);
                        text5.setText("Keracunan Staphylococcus aureus : " + s33 + "%\nKeracunan Jamur beracun : " + s34 + "%\nKeracunan Salmonellae : " + s35 + "%\nKeracunan Clostridium botulium : " + s36 + "%\nKeracunan Campylobacter : " + s37 + "%\n\nKesimpulan Penyakit : Anda sehat !!");
                    }

                    Button dialogButton = (Button) dialog.findViewById(R.id.bt_ok);

                    dialogButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });

                    dialog.show();
                }catch (Exception e){
                    final Dialog dialog = new Dialog(MainActivity.this);
                    dialog.setContentView(R.layout.muncul);
                    dialog.setTitle("TWOH.Co");

                    TextView text5 = (TextView) dialog.findViewById(R.id.tv_desc);
                    text5.setText("Input Value Threshold !");

                    Button dialogButton = (Button) dialog.findViewById(R.id.bt_ok);

                    dialogButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });

                    dialog.show();
                }
            }
        });
    }
}
